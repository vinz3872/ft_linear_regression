#!/usr/bin/env ruby

load 'Estimate.class.rb'

def main()
  if ARGV.size != 1
    puts "./estimate.rb mileage"
    return 0
  end
  estimation = Estimate.new(ARGV[0].to_i)

  estimation.run()
end

$file_theta = "theta.txt"

main()

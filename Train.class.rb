class Train
  attr_accessor :theta0
  attr_accessor :theta1
  attr_accessor :data
  attr_accessor :normalization


  def initialize()
    @theta0 = 0
    @theta1 = 0
    @data = Array.new
    @normalization = [0, 0]

    # load_theta()
  end

  # def load_theta()
  #   i = 0
  #   if File.exist?($file_theta) && File.file?($file_theta)
  #     File.readlines($file_theta).each do |line|
  #       @theta0 = line.to_f if i == 0
  #       @theta1 = line.to_f if i == 1
  #       i += 1
  #       return if i > 1
  #   	end
  #   end
  # end

  def save_theta()
    i = 0
    f = File.new($file_theta, File::CREAT|File::TRUNC|File::RDWR, 0644)
    f.write(@theta0.to_s)
    f.write("\n")
    f.write(@theta1.to_s)
    f.write("\n")
    f.write(@normalization[0].to_s)
    f.write("\n")
    f.write(@normalization[1].to_s)
    f.close()
  end

  def get_new_data_elem(mileage, price)
    data_elem = Struct.new(:mileage, :price)

    data_elem.new(mileage, price)
  end

  def parse_data()
    i = 0
    File.readlines($file_data).each do |line|
      i += 1
      next if i == 1

      res = line.split(",")
      if res.count != 2
        puts "Parse_data: Data error"
        return -1
      end
      @data << get_new_data_elem(res[0].to_f, res[1].to_f)
    end
  end

  def estimate(mileage, t0, t1)
    res = t0 + (t1 * mileage)

    res
  end

  def calc_gradient(t0, t1)
    arr = [0, 0]
    tmp0 = 0
    tmp1 = 0
    learning_rate = 1
    m = @data.count

    @data.each do |elem|
      tmp0 = (1.0/m) * (estimate(elem.mileage, t0, t1) - elem.price)
      tmp1 = (1.0/m) * ((estimate(elem.mileage, t0, t1) - elem.price) * elem.mileage)
      arr[0] = t0 - (learning_rate * tmp0)
      arr[1] = t1 - (learning_rate * tmp1)
      t0 = arr[0]
      t1 = arr[1]
    end


    arr
  end

  def apply_formula()
    arr = [0, 0]
    iter = 1000

    normalization = true
    if normalization == true
      max = (@data[0]).mileage
      min = (@data[0]).mileage
      @data.each do |elem|
        max = elem.mileage if elem.mileage > max
        min = elem.mileage if elem.mileage < min
      end
      @normalization[0] = min
      @normalization[1] = max
      # puts "min: " + min.to_s + " max: " + max.to_s
      @data.each do |elem|
        elem.mileage = (elem.mileage - max) / (max - min)
      end
    end

    while iter > 0
      arr = calc_gradient(arr[0], arr[1])
      iter -= 1
    end
    # puts arr[0], arr[1]
    @theta0 = arr[0]
    @theta1 = arr[1]

  end

  def run()
    i = 0
    if File.exist?($file_data) && File.file?($file_data)
      return if parse_data() == -1
      apply_formula()
      save_theta()
    else
      puts "no data.csv"
      return
    end
  end

end

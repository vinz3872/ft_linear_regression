#!/usr/bin/env ruby

load 'Train.class.rb'

def main()
  if ARGV.size != 0
    puts "./train.rb"
    return 0
  end
  train = Train.new()

  train.run()
end

$file_theta = "theta.txt"
$file_data = "data.csv"

main()

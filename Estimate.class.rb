class Estimate
  attr_accessor :theta0
  attr_accessor :theta1
  attr_accessor :mileage
  attr_accessor :normalization

  def initialize(mileage)
    @theta0 = 0
    @theta1 = 0
    @mileage = mileage
    @normalization = [0, 0]

    load_theta()
  end

  def load_theta()
    i = 0
    if File.exist?($file_theta) && File.file?($file_theta)
      File.readlines($file_theta).each do |line|
        @theta0 = line.to_f if i == 0
        @theta1 = line.to_f if i == 1
        @normalization[0] = line.to_f if i == 2
        @normalization[1] = line.to_f if i == 3
        i += 1
        return if i > 3
    	end
    else
      f = File.new($file_theta, File::CREAT|File::RDWR, 0644)
      f.write("0\n0")
      f.close()
    end
  end

  def run()
    tmp = (@mileage - @normalization[1]) / (@normalization[1] - @normalization[0])
    @mileage = tmp
    puts "Price : " + (@theta0 + (@theta1 * @mileage)).to_s
  end

end
